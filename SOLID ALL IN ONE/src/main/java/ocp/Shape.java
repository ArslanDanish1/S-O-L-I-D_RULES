package ocp;

/**
 * Created by AD on 11/21/2016.
 */

public abstract class Shape {
    abstract void draw();
}
