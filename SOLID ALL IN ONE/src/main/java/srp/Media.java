package srp;

/**
 * Created by AD on 11/21/2016.
 */

public abstract class Media {
    private String name;
    private int duration;

    public String getName(){

        return name;
    }

    public int getDuration(){

        return duration;
    }
    public void setName (String name)
    {
        this.name = name;
    }
    public void setDuration (int duration){

        this.duration= duration;
    }
    public void writeTo(Iwriter writer){

        writer.saveTo(name);
    }
    public abstract void printInfo();


}
